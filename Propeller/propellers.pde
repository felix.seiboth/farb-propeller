class propellers {



  float x = 0;
  float y = 0; //Position
  Color c;        //Farbe
  float blattl; 
  float blattb;


  // Constructor

  propellers( Color _c, float _blattb, float _blattl) {


    c = _c;
    blattb = _blattb;
    blattl = _blattl;
  }

  void setcolor(Color inputcolor) {

    c = inputcolor;
    return;
  }

  void drawSelf() {
    noStroke();
    fill(c.r,c.b,c.g);
    beginShape();
    ellipse(x, y, blattb, blattl);
    endShape();
  }
}
