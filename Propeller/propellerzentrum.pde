class propellerzentrum {


  // Attributes

  float x1, y1;
  float x2, y2;
  float x3, y3;
  Color c;




  propellerzentrum(float _x1, float _y1, float _x2, float _y2, float _x3, float _y3, Color _c) {

    x1 = _x1;
    y1 = _y1;
    x2 = _x2;
    y2 = _y2;
    x3 = _x3;
    y3 = _y3;
    c  = _c;
  }

  void setcolor(Color inputcolor) {

    c = inputcolor;
    return;
  }

  void drawSelf() {
    noStroke();
    fill(c.r,c.g,c.b);
    beginShape();
    triangle(x1, y1, x2, y2, x3, y3);
    endShape();
  }
}
