class propeller {

  propellers top;
  propellers left; 
  propellers right;
  propellerzentrum z;

  float x, y;
  float size;
  Color c;
  float fluegelb = 32;
  float fluegell = 160;
  float  r, g, b;



/*

  propeller(float _x, float _y, float _size, int _r, int _g, int _b) {

    x = _x;
    y = _y;
    size = _size;
    r = _r;
    g = _g;
    b = _b;
  }
  */
    propeller(float _x, float _y, float _size, Color _c) {

    x = _x;
    y = _y;
    size = _size;
    c = _c;
  }



  void setcolor(Color inputcolor) {
    top.setcolor(inputcolor);
    right.setcolor(inputcolor);
    left.setcolor(inputcolor);
    z.setcolor(inputcolor);

    return;
  }


  void drawSelf() {

    pushMatrix();
    //beginShape();
    translate(-356, -354);

    pushMatrix();
    beginShape();
    translate(300, 300);
    z = new propellerzentrum (30, 75, 58, 20, 86, 75, c);
    z.drawSelf();
    endShape();
    popMatrix();

    pushMatrix();
    beginShape();
    translate(358, 257);
    top = new propellers (c, fluegelb, fluegell);
    top.drawSelf();
    endShape();
    popMatrix();

    pushMatrix();
    beginShape();
    translate(441, 405);
    rotate(radians(120));

    left = new propellers (c, fluegelb, fluegell);
    left.drawSelf();
    endShape();
    popMatrix();

    pushMatrix();
    beginShape();
    translate(277, 406);
    rotate(radians(240));
    right = new propellers (c, fluegelb, fluegell);
    right.drawSelf();
    endShape();
    popMatrix();

    //endShape();
    popMatrix();
  }
}
